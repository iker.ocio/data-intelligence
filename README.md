### This repository is for Data Intelligence subject on MACC by Mondragon Unibertsitatea.

#### Repository owner is Iker Ocio

#### Filebeat + Node-RED

To startup Filebeat and Node-RED,

```sh
docker-compose up -d nodered filebeat
```

If you prefer to use Dockerfiles,

#### Filebeat Dockerfile

To build an image,

```sh
docker image build -t filebeat_arm:latest -f Dockerfile.beats .
```

To start container,

```sh
docker run --name filebeat --mount type=bind,source="$(pwd)"/node_red/data/export,target=/data/export -d filebeat_arm:latest /usr/share/filebeat/bin/filebeat -e -c /etc/filebeat/filebeat.yml -path.home /usr/share/filebeat -path.config /etc/filebeat -path.data /var/lib/filebeat -path.logs /var/log/filebeat
```


#### Filebeat compilation

To rebuild an ARM binaries with Filebeat, enter to ```beats_arm``` and,

```sh
./filebeat_compile_arm
```

